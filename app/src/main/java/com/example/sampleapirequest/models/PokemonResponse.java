package com.example.sampleapirequest.models;

import java.util.ArrayList;

public class PokemonResponse {
    private Integer count;
    private String next;
    private Object previous;
    private ArrayList<Pokemon> results = null;

    public Integer getCount() {
        return this.count;
    }
    public String getNext() {
        return this.next;
    }
    public ArrayList<Pokemon> getResults() {
        return this.results;
    }
}
