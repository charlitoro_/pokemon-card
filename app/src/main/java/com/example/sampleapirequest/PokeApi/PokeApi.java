package com.example.sampleapirequest.PokeApi;

import com.example.sampleapirequest.models.PokemonResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.GET;

public interface PokeApi {
    @GET("pokemon")
    Call<PokemonResponse> getPokemonList();
}
